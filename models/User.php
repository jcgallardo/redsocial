<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 17:17
 */

require_once('resources/database/connection.class.inc');

class User extends DataObject{
    protected $datos = array(
        "id" => "",
        "email" => "",
        "name" => "",
        "last_name" => "",
        "profile_photo" => "",
        "birthday" => "",
        "city" => "",
        "date_insert" => ""
    );

    public static function newUser($email, $password, $name, $last_name){
        $conexion = parent::conectar();
        $sql = "INSERT ".
                "into user(`email`,`name`,`last_name`,`password`)".
                "VALUES ".
                    "(:email, :name, :last_name, :password);";
        try{
            $st = $conexion->prepare($sql);
            $st->bindValue(':email',$email,PDO::PARAM_STR);
            $st->bindValue(':name',$name,PDO::PARAM_STR);
            $st->bindValue(':last_name',$last_name,PDO::PARAM_STR);
            $st->bindValue(':password',sha1($password),PDO::PARAM_STR);
            $st->execute();

            return Wrapper::wrap(200,'Usuario insertado');

        }catch(PDOException $e){
            parent::desconectar();
            die("ERROR INSERT USER: ".$e->getMessage());
        }
    }

    /**
     * @param $email
     * @param $password
     * @return array
     */
    public static function login($email, $password){
        $conexion = parent::conectar();
        $sql = "SELECT ".
                    "id, email, name, last_name, profile_photo ".
                "FROM ".
                    "user ".
                "WHERE ".
                    "email=:email ".
                    "AND password=:password";
        try{
            $st = $conexion->prepare($sql);
            $st->bindValue(':email',$email,PDO::PARAM_STR);
            $st->bindValue(':password', sha1($password), PDO::PARAM_STR);
            $st->execute();

            $fila = $st->fetch();

            parent::desconectar();

            if (!$fila){
                return Wrapper::wrap(400,'Usuario no encontrado');
            }else {
                $sql = "UPDATE user SET active=1 WHERE id=:id_user;";
                $st = $conexion->prepare($sql);
                $st->bindValue(':id_user', $fila['id']);
                $st->execute();

                return Wrapper::wrap(200, 'Usuario encontrado', array(
                        "user" => new User($fila))
                );
            }

        }catch(PDOException $e){
            parent::desconectar();
            die("Consulta login fallida: ".$e->getMessage());
        }
    }

    /**
     * @param $name
     * @param $lastname
     * @param $email
     * @param $pass
     * @return array
     */
    public static function registro($name, $lastname, $email, $pass){
        $conexion = parent::conectar();
        $sql = "INSERT INTO user ".
                    "(email, password, name, last_name) ".
                "VALUES ".
                    "(:email,:password,:name,:last_name);";
        try{
            $st = $conexion->prepare($sql);
            $st->bindValue(':email',$email);
            $st->bindValue(':password', sha1($pass));
            $st->bindValue(':name', $name);
            $st->bindValue(':last_name',$lastname);
            $insertado = $st->execute();

            parent::desconectar();

            if ($insertado)
                return Wrapper::wrap(200,'Usuario creado correctamente');
            else
                return Wrapper::wrap(400, 'Error al crear el usuario');

        }catch(PDOException $e){
            parent::desconectar();
            return Wrapper::wrap(400, 'Error al crear el usuario; email ya existente');
        }
    }

    public static function closeSession($id){
        $conexion = parent::conectar();
        $sql =
            "UPDATE user ".
                "SET active=0 ".
            "WHERE ".
                "id=:id;";
        try{
            $st = $conexion->prepare($sql);
            $st->bindValue(':id',$id);
            $updated = $st->execute();

            if ($updated)
                return Wrapper::wrap(200,'Sesión finalizada correctamente');
            else
                return Wrapper::wrap(400,'Error al finalizar la sesión');

        }catch(PDOException $e){
            parent::desconectar();
            return Wrapper::wrap(400, 'Error SQL');
        }
    }

    public static function getAllUsers($excludedUserId = null)
    {
        $conexion = parent::conectar();
        $sql = "SELECT " .
            "id, email, name, last_name, profile_photo " .
            "FROM " .
            "user ";
        if ($excludedUserId != null) {
            $sql .= "WHERE " .
                "id!=:id";
        }

        try {
            $st = $conexion->prepare($sql);

            if ($excludedUserId != null)
                $st->bindValue(':id', $excludedUserId);

            $st->execute();

            $users  = array();
            foreach ($st->fetchAll() as $usuario) {
                $users[] = new User($usuario); //Asumiendo que existe una clase Usuario.
            };

            return Wrapper::wrap(200,'Lista de usuarios leída correctamente', array(
                "usuarios" => $users
            ));

        } catch (PDOException $e) {
            parent::desconectar();
            return Wrapper::wrap(400, 'Error al consultar usuarios');
        }
    }

    public static function getActiveUsers()
    {
        $conexion = parent::conectar();
        $sql =
            "SELECT " .
                "id, email, name, last_name, profile_photo " .
            "FROM ".
                "user ".
            "WHERE ".
                "active=1;";

        try {
            $st = $conexion->prepare($sql);
            $st->execute();

            $usuarios = array();

            foreach ($st->fetchAll() as $usuario) {
                $usuarios[] = new User($usuario); //Asumiendo que existe una clase Usuario.
            }

            return Wrapper::wrap(200,'Lista de usuarios leída correctamente', array(
                "active_users" => $usuarios
            ));

        } catch (PDOException $e) {
            parent::desconectar();
            return Wrapper::wrap(400, 'Error al consultar active users');
        }
    }

    public static function getUserById($id_user)
    {
        $conexion = parent::conectar();
        $sql =
            "SELECT " .
                "id, email, name, last_name, profile_photo, DATE_FORMAT(date_insert, '%d de %b de %Y') as date_insert, DATE_FORMAT(birthday, '%d/%m/%Y') as birthday " .
            "FROM ".
                "user ".
            "WHERE ".
                "id=:id_user;";

        try {
            $st = $conexion->prepare($sql);
            $st->bindValue(':id_user',$id_user);
            $st->execute();

            $usuario = null;

            if ($st->rowCount() > 0)
                $usuario = new User($st->fetch());

            parent::desconectar();

            if ($usuario) {
                return Wrapper::wrap(200, 'Usuario con id ' . $id_user . ' leído correctamente', array(
                    "usuario" => $usuario
                ));
            }else{
                return Wrapper::wrap(400, 'Usuario no existe');
            }

        } catch (PDOException $e) {
            parent::desconectar();
            return Wrapper::wrap(400, 'Error SQL (User::getUserById)');
        }
    }

    /**
     * @param $id_user
     * @param $new_name
     * @param $new_lastname
     * @param $new_email
     */
    public static function updateProfile($id_user, $new_name, $new_lastname, $new_email)
    {
        $conexion = parent::conectar();
        $sql =
            "UPDATE " .
                "user ".
            "SET ".
                "name=:name, last_name=:last_name, email=:email ".
            "WHERE ".
                "id=:id_user;";

        try {
            $st = $conexion->prepare($sql);
            $st->bindValue(':name',$new_name);
            $st->bindValue(':last_name',$new_lastname);
            $st->bindValue(':email',$new_email);
            $st->bindValue(':id_user',$id_user);
            $st->execute();
            parent::desconectar();

            if ($st->rowCount()) {
                return Wrapper::wrap(200, 'Tus datos se han modificado correctamente');
            }else{
                return Wrapper::wrap(400,'Error al modificar el usuario');
            }

        } catch (PDOException $e) {
            parent::desconectar();
            return Wrapper::wrap(400, 'Error SQL (User::getUserById)');
        }
    }
}