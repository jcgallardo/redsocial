<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 03/05/2017
 * Time: 19:54
 */

require_once('resources/database/connection.class.inc');
require_once('models/Comentario.php');

class Entrada extends DataObject
{
    protected $datos = array(
        "id" => "",
        "title" => "",
        "content" => "",
        "photo" => "",
        "date_insert" => "",
        "user" => ""
    );

    static function getAllSimpleEntradas(){
        $conexion = parent::conectar();
        $sql = "SELECT ".
                "entrada.id as id, entrada.title as title, entrada.content as content, entrada.photo as photo, entrada.date_insert as date_insert, user.id as user_id, user.name as user_name, user.last_name as user_last_name, user.profile_photo as user_photo ".
            "FROM ".
                "entrada, user ".
            "WHERE ".
                "user.id=entrada.id_user ".
            "ORDER BY ".
                "entrada.date_insert DESC";
        try{
            $st = $conexion->prepare($sql);
            $st->execute();

            $entradas = array();

            foreach ($st->fetchAll() as $entradaCompleta) {
                $user = array(
                    "id" => $entradaCompleta['user_id'],
                    "name" => $entradaCompleta['user_name'],
                    "last_name" => $entradaCompleta['user_last_name'],
                    "profile_photo" => $entradaCompleta['user_photo']
                );
                $nuevoUser = new User($user);
                $entrada = array(
                    "id" => $entradaCompleta['id'],
                    "title" => $entradaCompleta['title'],
                    "content" => $entradaCompleta['content'],
                    "photo" => $entradaCompleta['photo'],
                    "date_insert" => $entradaCompleta['date_insert'],
                    "user" => $nuevoUser
                );

                $entradas[] = new Entrada($entrada); //Asumiendo que existe una clase Fruta.
            };

            return Wrapper::wrap(200,'Entradas encontradas',array(
                "entradas" => $entradas
            ));

        }catch(PDOException $e){
            parent::desconectar();
            die("ERROR SELECT entradas: ".$e->getMessage());
        }
    }

    /**
     * @param $id_user
     * @return array
     */
    public static function getSimpleEntradasByIdUser($id_user)
    {
        $conexion = parent::conectar();
        $sql =
            "SELECT ".
                "entrada.id as id, entrada.title as title, entrada.content as content, entrada.photo as photo, entrada.date_insert as date_insert, user.id as user_id, user.name as user_name, user.last_name as user_last_name, user.profile_photo as user_photo ".
            "FROM ".
                "entrada, user ".
            "WHERE ".
                "entrada.id_user=:id_user AND user.id=entrada.id_user ".
            "ORDER BY ".
                "entrada.date_insert DESC";

        try {
            $st = $conexion->prepare($sql);
            $st->bindValue(':id_user',$id_user);
            $st->execute();

            $entradas = array();

            foreach ($st->fetchAll() as $entradaCompleta) {
                $user = array(
                    "id" => $entradaCompleta['user_id'],
                    "name" => $entradaCompleta['user_name'],
                    "last_name" => $entradaCompleta['user_last_name'],
                    "profile_photo" => $entradaCompleta['user_photo']
                );
                $nuevoUser = new User($user);
                $entrada = array(
                    "id" => $entradaCompleta['id'],
                    "title" => $entradaCompleta['title'],
                    "content" => $entradaCompleta['content'],
                    "photo" => $entradaCompleta['photo'],
                    "date_insert" => $entradaCompleta['date_insert'],
                    "user" => $nuevoUser
                );

                $entradas[] = new Entrada($entrada); //Asumiendo que existe una clase Fruta.
            };

            parent::desconectar();

            return Wrapper::wrap(200, 'Entradas encontradas', array(
                "entradas" => $entradas
            ));
        }catch(PDOException $e){
            parent::desconectar();
            die("ERROR SELECT entradas by ID_USER: ".$e->getMessage());
        }
    }

    /**
     * @param $id_entrada
     * @return array
     */
    public static function getEntradaById($id_entrada)
    {
        $conexion = parent::conectar();
        $sql =
            "SELECT ".
                "entrada.id as id, entrada.title as title, entrada.content as content, entrada.photo as photo, entrada.date_insert as date_insert, user.id as user_id, user.name as user_name, user.last_name as user_last_name, user.profile_photo as user_photo ".
            "FROM ".
                "entrada, user ".
            "WHERE ".
                "entrada.id=:id_entrada AND user.id=entrada.id_user ";

        try {
            $st = $conexion->prepare($sql);
            $st->bindValue(':id_entrada',$id_entrada);
            $st->execute();

            if ($entradaCompleta = $st->fetch()) {
                echo "<br>";
                $user = array(
                    "id" => $entradaCompleta['user_id'],
                    "name" => $entradaCompleta['user_name'],
                    "last_name" => $entradaCompleta['user_last_name'],
                    "profile_photo" => $entradaCompleta['user_photo']
                );
                $nuevoUser = new User($user);
                $mientrada = array(
                    "id" => $entradaCompleta['id'],
                    "title" => $entradaCompleta['title'],
                    "content" => $entradaCompleta['content'],
                    "date_insert" => $entradaCompleta['date_insert'],
                    "photo" => $entradaCompleta['photo'],
                    "user" => $nuevoUser
                );

                $entrada = new Entrada($mientrada); //Asumiendo que existe una clase Fruta.
            };

            parent::desconectar();
            return Wrapper::wrap(200, 'Entradas encontradas', array(
                "entrada" => $entrada
            ));
        }catch(PDOException $e){
            parent::desconectar();
            die("ERROR SELECT entradas by ID_USER: ".$e->getMessage());
        }
    }

    /**
     * @param $otro_usuario
     */
    public static function getOnlyFotos($usuario)
    {
        $conexion = parent::conectar();
        $sql =
            "SELECT ".
                "id, photo ".
            "FROM ".
                "entrada ".
            "WHERE ".
                "id_user=:usuario";
        try{
            $st = $conexion->prepare($sql);
            $st->bindValue(':usuario',$usuario);
            $st->execute();

            $fotos = array();

            foreach ($st->fetchAll() as $entrada) {
                if ($entrada['photo'])
                    $fotos[] = [
                        "id" => $entrada['id'],
                        "photo" => $entrada['photo']
                    ];
            };

            return Wrapper::wrap(200,'Fotos encontradas',array(
                "fotos" => $fotos
            ));

        }catch(PDOException $e){
            parent::desconectar();
            die("ERROR SELECT fotos: ".$e->getMessage());
        }
    }

    /**
     * @param $id_user
     * @param $title_entrada
     * @param $description_entrada
     * @return array
     */
    public static function newEntrada($id_user, $title_entrada, $description_entrada)
    {
        $conexion = parent::conectar();
        $sql =
            "INSERT INTO ".
                "entrada (`title`, `content`,`id_user`) ".
            "VALUES ".
                "(:title_entrada, :description_entrada, :id_user);";

        $st = $conexion->prepare($sql);
        $st->bindValue(':id_user',$id_user);
        $st->bindValue(':title_entrada', $title_entrada);
        $st->bindValue(':description_entrada', $description_entrada);
        $st->execute();
        parent::desconectar();

        if ($conexion->lastInsertId() > 0)
            return Wrapper::wrap(200,'Entrada insertada');
        else
            return Wrapper::wrap(400,'No se ha podido insertar la entrada');
    }

    /**
     * @param $id_entrada
     */
    public static function getComplexEntradaById($id_entrada)
    {
        $conexion = parent::conectar();

        // traemos los datos de la entrada
        $resultado_entrada = self::getEntradaById($id_entrada);
        if($resultado_entrada['code'] == 200){
            // traemos los comentarios de la entrada.
            $sql =
                'SELECT '.
                    'c.id_entrada, u.id as id_user, u.name as name_user, u.profile_photo as photo_user, c.description, c.date_insert '.
                'FROM '.
                    'comentarios c, user u '.
                'WHERE '.
                    'c.id_entrada=:id_entrada AND c.id_user=u.id '.
                'ORDER BY '.
                    'c.date_insert DESC';

            $st = $conexion->prepare($sql);
            $st->bindValue(':id_entrada',$id_entrada);
            $st->execute();

            $comentarios = array();
            foreach ($st->fetchAll() as $fila){
                $comentarios[] = new Comentario($fila);
            }

            parent::desconectar();
            return Wrapper::wrap(200, "Entrada leída correctamente",[
                "entrada" => $resultado_entrada['data']['entrada'],
                "comentarios" => $comentarios
            ]);
        }else{
            parent::desconectar();
            return Wrapper::wrap(400, $resultado_entrada['description']);
        }


    }
}