<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 14/05/2017
 * Time: 19:59
 */
require_once('resources/database/connection.class.inc');

class Comentario extends DataObject
{
    protected $datos = array(
        "id_entrada" => "",
        "id_user" => "",
        "name_user" => "",
        "photo_user" => "",
        "description" => "",
        "date_insert" => ""
    );

    /**
     * @param $devolverValor
     * @param $description
     */
    public static function mewComment($id_entrada, $id_user, $description)
    {
        $conexion = parent::conectar();
        $sql =
            "INSERT INTO ".
                "comentarios (`id_entrada`, `id_user`,`description`) ".
            "VALUES ".
                "(:id_entrada, :id_user, :description);";

        $st = $conexion->prepare($sql);
        $st->bindValue(':id_entrada',$id_entrada);
        $st->bindValue(':id_user', $id_user);
        $st->bindValue(':description', $description);
        $st->execute();
        parent::desconectar();

        if ($conexion->lastInsertId() > 0)
            return Wrapper::wrap(200,'Comentario insertado');
        else
            return Wrapper::wrap(400,'No se ha podido insertar el comentario');
    }
}

