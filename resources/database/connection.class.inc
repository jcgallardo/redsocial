<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 16:43
 */

require_once("connection.inc");

$connection = new PDO(DB_DSN, DB_USUARIO, DB_PASSWORD);

abstract class DataObject
{
    protected $datos = array();

    public function __construct($datos)
    {
        foreach ($datos as $clave => $valor)
            if (array_key_exists($clave, $this->datos)) $this->datos[$clave] = $valor;
    }

    public function devolverValor($campo)
    {
        if (array_key_exists($campo, $this->datos))
            return $this->datos[$campo];
        else
            die('Campo no encontrado');
    }

    protected static function conectar()
    {
        try{
            $conexion = new PDO(DB_DSN, DB_USUARIO, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            // Permitimos a PHP que la conexión se quede abierta para poderla utilizar en otras partes de la app.
            $conexion->setAttribute(PDO::ATTR_PERSISTENT, true);
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            die('Conexión fallida: '.$e->getMessage());
        }
        return $conexion;
    }

    protected static function desconectar(){
        $conexion = "";
    }
}