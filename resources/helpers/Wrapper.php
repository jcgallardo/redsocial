<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 20:55
 */
class Wrapper
{
    public static function wrap($errorCode,$description,$result=null){
        return array(
            "code" => $errorCode,
            "description" => $description,
            "data" => $result
        );
    }
}