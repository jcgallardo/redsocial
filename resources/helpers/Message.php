<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 14/05/2017
 * Time: 15:28
 */
class Message
{
    static function printMessage($result){
        if ($result['code']==200)
            self::printSuccess($result['description']);
        else
            self::printError($result['description']);
    }
    static function printError($message){
        $html = '
            <p class="message message-error">'.$message.'</p>
        ';
        echo $html;
    }
    static function printSuccess($message){
        $html = '
            <p class="message message-success">'.$message.'</p>
        ';
        echo $html;
    }
}