<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 16:33
 */
session_start();
header("Content-Type: text/html;charset=utf-8");

include('resources/database/connection.class.inc');
include('resources/config/config.inc');
include('resources/helpers/wrapper.php');
include('resources/helpers/message.php');
require_once('models/User.php');
include('views/MainView.php');

echo "<!DOCTYPE html>";
echo "<html lang='es'>";
MainView::printHead("Socialty");

echo "<body>";

//////////////////////////////////////////////

$logeado = false;
$user = "";
$error_login = "";
$error_registro = "";

if (isset($_SESSION['user'])) {
    $user = unserialize($_SESSION['user']);
    $logeado = true;
}

if ($logeado) {
    if (isset($_GET['b_accion']) && $_GET['b_accion'] == 'salir') {
        unset($_SESSION['user']);
        User::closeSession($user->devolverValor('id'));
        $logeado = false;
    }
}else{ // no logeado
    // si pulsamos sobre logear iniciamos sesion y guardamos el usuario en SESSION
    if (isset($_POST['b_login'])) {
        $email = $_POST['i_email'];
        $password = $_POST['i_pass'];

        $resultado = User::login($email, $password);

        if ($resultado['code'] == 200) {
            $logeado = true;
            $_SESSION['user'] = serialize($resultado['data']['user']);
            $user = unserialize($_SESSION['user']);
        } else {
            $error_login = $resultado['description'];
        }
    }

    // si pulsamos sobre registrar insertamos el usuario y luego lo logeamos
    if (isset($_POST['b_registro'])) {
        $name = $_POST['reg-name'];
        $lastname = $_POST['reg-lastname'];
        $email = $_POST['reg-email'];
        $pass1 = $_POST['reg-pass'];
        $pass2 = $_POST['reg-pass2'];

        if ($pass1 == $pass2) {
            $resultadoRegistro = User::registro($name, $lastname, $email, $pass1);
            if ($resultadoRegistro['code'] == 200) {
                $resultadoLogin = User::login($email, $pass1);
                if ($resultadoLogin['code'] == 200) {
                    $logeado = true;
                    $_SESSION['user'] = serialize($resultadoLogin['data']['user']);
                    $user = unserialize($_SESSION['user']);
                } else {
                    $error_login = $resultadoLogin['description'];
                }
            } else {
                $error_registro = $resultadoRegistro['description'];
            }
        } else {
            $error_registro = "Las contraseñas no coinciden";
        }
    }
}

if (!$logeado){
    include('controllers/noAuth.php');
}else{
    include('controllers/auth.php');
}

////////////////////////////////////////////////

MainView::printFooter();

echo "</body></html>";