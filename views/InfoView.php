<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 19:56
 */
class InfoView
{

    public static function printInfo($user)
    {
        $html =
            '<section class="informacion">
                <article>
                    <h1>Échale un vistazo a la información más relevante</h1>
                    <img class="foto-completa" alt="icono-perfil-users" src="'.PATH_IMAGES.'/'.$user->devolverValor("profile_photo").'"/>
                    <p><strong> Nombre: </strong>'.$user->devolverValor("name").'</p>
                    <p><strong> Apellidos: </strong>'.$user->devolverValor("last_name").'</p>
                    <p><strong> Email: </strong>'.$user->devolverValor("email").'</p>
                    <p>Registrado el '.$user->devolverValor("date_insert").'</p>
                </article>
            </section>';
        echo $html;
    }

    public static function printInfoEditable($user, $action)
    {
        $html =
            '<article>
                <h1>Échale un vistazo a tu información más relevante</h1>
                <img class="foto-completa" alt="icono-perfil-users" src="'.PATH_IMAGES.'/'.$user->devolverValor("profile_photo").'"/>
                <form action="'.$action.'" method="post">
                    <p><strong> Nombre: </strong><input name="name" type="text" value="'.$user->devolverValor("name").'" required /></p>
                    <p><strong> Apellidos: </strong><input name="last_name" type="text" value="'.$user->devolverValor("last_name").'" required /></p>
                    <p><strong> Email: </strong><input name="email" type="email" value="'.$user->devolverValor("email").'" required /></p>
                    <p>Registrado el '.$user->devolverValor("date_insert").'</p>
                    <p><strong> Fecha de nacimiento: </strong>'.$user->devolverValor("birthday").'</p>
                    <!--<input type="file" placeholder="sube tu foto de perfil" required>-->
                    <button type="submit" name="b_accion" value="update_profile" class="miboton">Actualizar perfil</button>
                </form>
            </article>';
        echo $html;
    }
}