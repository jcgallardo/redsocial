<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 18:17
 */
class FotosView
{
    static function printGaleria($galeria){
        $html =
            '<section class="cont-fotos">
                <section class="fotos">
                    <h1>Galería de fotos obtenida de las entradas publicadas</h1>
                    ';

                    foreach ($galeria as $foto){
                        $html .= '<a href="?index.php?section=foto&id_entrada='.$foto['id'].'"><img alt="icono-perfil-users" src="'.PATH_IMAGES."/".$foto['photo'].'"/></a>';
                    }
                    $html .=
                '</section>
            </section>';
        echo $html;
    }
}