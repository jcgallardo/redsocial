<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 18:53
 */
class PortadaView
{
    public static function printNewEntrada(){
        $html =
            '<section class="nueva-entrada">
                    <form action="index.php?section=portada" method="post">
                    <h1>Publica una entrada nueva</h1>
                    <input type="text" name="title_entrada" class="input-default" placeholder="Introduce el título de la entrada" />
                    <textarea name="description_entrada" placeholder="Escribe aquí tu comentario..."></textarea>
                    <button name="b_accion" value="new_entrada" class="miboton" type="submit">Publicar</button>
                    </form>
                </section>';
        echo $html;
    }

    public static function printEntradas($entradas)
    {
        $html =
            '<h1>Échale un vistazo a las entradas más recientes...</h1>';

        foreach ($entradas as $entrada){
            $tiempo_entrada = new DateTime($entrada->devolverValor('date_insert'));
            $tiempo_actual = new DateTime();

            $interval = date_diff($tiempo_entrada,$tiempo_actual)->format('%R%a días');

            $user = $entrada->devolverValor('user');
            $html .= '<article>
                        <a href="index.php?section=entrada&id='.$entrada->devolverValor("id").'">
                            <section class="info-user-entradas">
                                <img alt="icono-perfil-users" src="'.PATH_IMAGES."/".$user->devolverValor("profile_photo").'"/>
                                <p class="user-name">'.$user->devolverValor("name").' '. $user->devolverValor("last_name").'</p>
                                <p class="tiempo">Hace '.$interval.'</p>
                            </section>
                            <h1>'.$entrada->devolverValor("title").'<h1>
                            <p>'.$entrada->devolverValor("content").'</p>
                        </a>
                    </article>';
        }

        echo $html;
    }
}