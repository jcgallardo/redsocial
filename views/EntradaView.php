<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 15/05/2017
 * Time: 20:08
 */
class EntradaView
{

    /**
     * @param $entrada
     */
    public static function printEntrada($entrada)
    {
        $html =
            '<section class="entrada-completa">
                    <article>
                        <section class="info-user-entradas">
                            <img alt="icono-perfil-users" src="'.PATH_IMAGES.'/'.$entrada->devolverValor('user')->devolverValor('profile_photo').'"/>
                            <p class="user-name">'.$entrada->devolverValor('user')->devolverValor('name').'</p>
                            <p class="tiempo">'.$entrada->devolverValor('date_insert').'</p>
                        </section>
                        <h1>'.$entrada->devolverValor('title').'</h1>
                        <p class="p-contenido">'.$entrada->devolverValor('content').'</p>';
                        if ($entrada->devolverValor('photo')!=null)
                            $html .= '<img class="imagen-entrada" src="'.PATH_IMAGES.'/'.$entrada->devolverValor('photo').'" alt="friends" />';
                    $html .= '</article>
                </section>';

        echo $html;
    }

    /**
     * @param $comentarios
     * @param $main_user
     */
    public static function printComentarios($comentarios, $main_user, $action){
        $html =
            '<section class="comentarios">
            <h2>Comentarios</h1>';

            $html .= self::printFormComentario($main_user, $action);

            if (count($comentarios) > 0) {
                foreach ($comentarios as $comentario) {
                    $html .= self::printComentario($comentario);
                }
            }else{
                $html .= '<article>Aún no hay comentarios para esta entrada, ¡se el primero en comentar!</article>';
            }

            $html .= '</section>';
        echo $html;
    }

    /**
     * @param $comentario
     * @return string
     */
    public static function printComentario($comentario){
        $html =
            '<article>
                <img class="foto-mediana" alt="icono-perfil-users" src="'.PATH_IMAGES.'/'.$comentario->devolverValor('photo_user').'"/>
                <p class="user-name">'.$comentario->devolverValor('name_user').'</p>
                <p class="tiempo">'.$comentario->devolverValor('date_insert').'</p>
                <p class="cont-comentario">'.$comentario->devolverValor('description').'</p>
            </article>';

        return $html;
    }

    /**
     * @param $mainUser
     */
    public static function printFormComentario($mainUser, $action){
        $html =
            '<section class="nuevo-comentario">
                <form method="post" action="'.$action.'">
                <img class="foto-mediana" alt="icono-perfil-users" src="'.PATH_IMAGES.'/'.$mainUser->devolverValor('profile_photo').'"/>
                <textarea name="description" placeholder="Escribe tu comentario aquí"></textarea>
                <button name="b_accion" value="new_comment" type="submit" class="miboton">Comentar</button>
                </form>
            </section>';
        return $html;
    }

}