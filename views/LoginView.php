<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 20:28
 */
class LoginView
{
    public static function login($mensajeLogin=null, $mensajeRegistro=null){
        $html = '
            <header>
                <section>
                    <img width="160" height="60" class="logo" src="'.PATH_IMAGES.'/logo.png" alt="logo Socialty" />
                </section>
                <section>
                    <h1>Socialty</h1>
                </section>
                <section class="form-login">
                    <form method="post" action="index.php">
                        <ul class="login-fijo">
                            <li><input type="email" placeholder="Ej: example@ugr.es" value="juan@ugr.es" name="i_email" required /></li>
                            <li><input type="password" placeholder="Su contraseña" value="********" name="i_pass" required /></li>
                            <li><button class="miboton miboton-login"type="submit" name="b_login" value="login">Entrar</button></li>
                        </ul>
                        ';

        if ($mensajeLogin != ""){
            $html .= '<p class="error-login">ERROR - '.$mensajeLogin.'</p>';
        }

        $html  .=  '
                    </form>
                </section>
                <section class="icono-login">
                    <form method="post" action="index.php">
                        <input type="checkbox" id="toggle">
                        <label for="toggle">Entrar</label>
                        ';
        if ($mensajeLogin != ""){
            $html .= '<p class="error-login">ERROR - '.$mensajeLogin.'</p>';
        }
        $html .= '
                        <ul class="login-emergente">
                            <li>Inicia sesión</li>
                            <li><input type="email" placeholder="Ej: example@ugr.es" value="juan@ugr.es" name="i_email" required /></li>
                            <li><input type="password" placeholder="Su contraseña" value="********" name="i_pass" required /></li>
                            <li><button class="miboton miboton-login" type="submit" name="b_login" value="login">Iniciar sesión</button></li>
                        </ul>
                    </form>
                </section>
            </header>
            <section class="contenido">
                <section class="sec-portada portada">
                    <h1>Conéctate al mundo. Disfruta. Imagina. Presume. Vuela. <span class="great">¡Vive!</span></h1>
                </section>
                <section class="sec-portada">
                    <h2>Únete a nuestra red más social</h2>
                    <form id="form-registro" class="form-registro" method="post" action="index.php">
                        <label for="id-reg-name">Nombre</label>
                        <input type="text" id="id-reg-name" name="reg-name" placeholder="Ejemplo: Ana" required />
                        <label for="id-reg-lastname">Apellidos</label>
                        <input type="text" id="id-reg-lastname" name="reg-lastname" placeholder="Ejemplo: García" required />
                        <label for="id-reg-email">E-mail</label>
                        <input type="text" id="id-reg-email" name="reg-email" placeholder="Ejemplo: example@gmail.com" required/>
                        <label for="id-reg-pass">Contraseña</label>
                        <input type="password" id="id-reg-pass" name="reg-pass" placeholder="Su contraseña" required />
                        <label for="id-reg-pass2">Repita su contraseña</label>
                        <input type="password" id="id-reg-pass2" name="reg-pass2" placeholder="Su contraseña de nuevo" required />
                        ';
        if ($mensajeRegistro != ""){
            $html .= '<p class="error-registro">ERROR - '.$mensajeRegistro.'</p>';
        }
        $html .= '
                        <button class="miboton" type="submit" name="b_registro" value="registro">Registrarse</button>
                    </form>
                </section>
            </section>
        ';
        echo $html;
    }
}