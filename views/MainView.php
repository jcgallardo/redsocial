<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 20:18
 */
class MainView
{
    public static function printHead($title){
        $html = '
            <head>
                <link href="public/stylesheet/style.css" rel="stylesheet" />
                <title>'.$title.'</title>
                <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, user-scalable=no" />
            </head>';

        echo $html;
    }

    public static function printFooter(){
        $html = '
            <footer class="pie">
                <ul>
                    <li><a href="">Contacto</a></li>
                    <li><a href="">Acerca de</a></li>
                </ul>
                <p>&copy 2017 Juan Carlos Gallardo Morales <br><strong> Universidad de Granada</strong></p>
            </footer>
        ';
        echo $html;
        ;
    }
}