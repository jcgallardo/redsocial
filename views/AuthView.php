<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 03/05/2017
 * Time: 18:52
 */
class AuthView
{
    /**
     * @param $nombreUsuario
     * @param $fotoUsuario
     */
    static function printHeader($nombreUsuario, $fotoUsuario, $seccion){
        $html = '<header>
            <section>
                <a href="index.php?section=portada">
                    <img width="160" height="60" class="logo" src="'.PATH_IMAGES.'/logo.png" alt="logo Socialty" />
                </a>
            </section>
            <section>
                <a href="index.php?section=portada"><h1>Socialty</h1></a>
            </section>
            <section class=\'user\'>
                <img class=\'icono-perfil\' alt=\'icono de perfil\' src="'.PATH_IMAGES."/".$fotoUsuario.'" />
                <p><a href="index.php?section='.$seccion.'">'.$nombreUsuario.'</a> <a class=\'exit\' href="index.php?b_accion=salir">Salir</a></p>
            </section>
            <section class=\'user-small\'>
                <input type="checkbox" id="toggle-user">
                <label for="toggle-user"><img class=\'icono-perfil\' alt=\'icono de perfil\' src=\'jcgallardo.jpg\' /></label>
                <ul class="user-emergente">
                    <li><a href="miperfil.html">Mi perfil</a></li>
                    <li><a class=\'exit\' href="index.php?b_accion=salir">Salir</a></li>
                </ul>
            </section>
        </header>';
        echo $html;
    }

    /**
     *
     */
    static function printNav($seccion, $user = null){
        $enlace_bio = 'index.php?section=biografia';
        $enlace_fotos = 'index.php?section=fotos';
        $enlace_info = 'index.php?section=info';
        $bio_class='';
        $fotos_class='';
        $info_class='';

        if ($user != null){
            $enlace_bio .= '&user='.$user->devolverValor('id');
            $enlace_fotos .= '&user='.$user->devolverValor('id');
            $enlace_info .= '&user='.$user->devolverValor('id');
        }

        switch($seccion){
            case "biografia" : $bio_class='class="active"'; break;
            case "fotos" :  $fotos_class='class="active"'; break;
            case "info" : $info_class='class="active"';break;
        }

        $html =
            '<nav class="menu-navegacion">
                <ul>';
                    if ($user != null)
                        $html .= '<li class="info-perfil">Perfil de '.$user->devolverValor('name').'</li>';
                    $html .=
                        '<li></li>
                        <li><a '.$bio_class.' href="'.$enlace_bio.'">Biografía</a></li>
                        <li><a '.$fotos_class.' href="'.$enlace_fotos.'">Fotos</a></li>
                        <li><a '.$info_class.' href="'.$enlace_info.'">Información</a></li>
                </ul>
            </nav>';

        echo $html;
    }

    /**
     * @param $users
     */
    static function printUsers($users, $section){
        $html = '
            <section class=\'contenido contenido_portada\'>
                <section class=\'users\'>
                    <ul>
                        ';
                        // se repite por cada usuario

        foreach ($users as $user){
            $html .= '<a href="index.php?section='.$section.'&user='.$user->devolverValor('id').'">
                            <li>
                                <p>'.$user->devolverValor('name').'</p>
                                <img alt=\'icono-perfil-users\' src="'.PATH_IMAGES."/".$user->devolverValor("profile_photo").'"/>
                            </li>
                        </a>';
        }

        // se cierra todo
        $html .= '</ul>
                </section>';

        echo $html;
    }

    /**
     * @param $users
     */
    static function printActiveUsers($active_users)
    {
        $html =
        '<aside class="users-activos">
                <h1>Usuarios activos</h1>
                <ul>';
        foreach ($active_users as $user) {
            $html .= '<li><a href="index.php?section=biografia&user='.$user->devolverValor('id').'"><img alt="icono-perfil-users" src="' . PATH_IMAGES . "/" . $user->devolverValor('profile_photo') . '"/><span>' . $user->devolverValor('name') . '</span></a></li>';
        }
        $html .=
                '</ul>
        </aside>';

        echo $html;
    }
}