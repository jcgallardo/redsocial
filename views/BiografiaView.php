<?php

/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 03/05/2017
 * Time: 19:44
 */
class BiografiaView
{
    static function printEntradas($entradas){
        $html =
            '<section class="entradas">
                    <h1>Entradas de la biografía</h1>';
            foreach ($entradas as $entrada){
                $user = $entrada->devolverValor('user');
                $html .= '<article>
                        <a href="index.php?$section=entrada&id='.$entrada->devolverValor("id").'">
                            <section class="info-user-entradas">
                                <img alt="icono-perfil-users" src="'.PATH_IMAGES."/".$user->devolverValor("profile_photo").'"/>
                                <p class="user-name">'.$user->devolverValor("name").' '. $user->devolverValor("last_name").'</p>
                                <p class="tiempo">'.$entrada->devolverValor("date_insert").'</p>
                            </section>
                            <h1>'.$entrada->devolverValor("title").'<h1>
                            <p>'.$entrada->devolverValor("content").'</p>
                        </a>
                    </article>';
            }
        $html .= '</section>';

        echo $html;
    }
}