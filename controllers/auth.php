<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 01/05/2017
 * Time: 20:53
 */

include('views/AuthView.php');

$seccion = isset($_GET['section']) ? $_GET['section'] : null;
$otro_usuario_id = isset($_GET['user']) ? $_GET['user'] : null;
$otro_user = null;


// pintamos el header con el nombre de usuario y la imagen
AuthView::printHeader($user->devolverValor('name'), $user->devolverValor('profile_photo'), $seccion);

if ($otro_usuario_id) {
    $resultado_user = User::getUserById($otro_usuario_id);
    if ($resultado_user['code']==200)
        $otro_user = $resultado_user['data']['usuario'];
    else
        die('El usuario indicado no existe');
}

if ($otro_usuario_id && $otro_user) {
    AuthView::printNav($seccion, $otro_user);
    // pintamos el menú de navegación
}else {
    AuthView::printNav($seccion);
}


// nos traemos todos los usuarios salvo el nuestro
$resultado = User::getAllUsers($user->devolverValor('id'));
// nos traemos todos los usuarios activos
$r_active_users = User::getActiveUsers();

// Pintamos toda la cabecera de usuarios
AuthView::printUsers($resultado['data']['usuarios'], $seccion);
echo '<section class="content">';
AuthView::printActiveUsers($r_active_users['data']['active_users']);
// Pintamos todo el menú lateral

$seccion = "";
if (isset($_GET['section']))
    $seccion = $_GET['section'];

switch ($seccion) {
    case "":
    case "portada":
        include('controllers/auth/portada.php');
        break;
    case "biografia":
        include('controllers/auth/biografia.php');
        break;
    case "fotos":
        include('controllers/auth/fotos.php');
        break;
    case "info":
        include('controllers/auth/info.php');
        break;
    case "entrada":
        include('controllers/auth/entrada.php');
        break;
}
echo '</section>';



