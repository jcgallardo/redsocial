<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 18:51
 */

include('models/Entrada.php');
include('views/PortadaView.php');

$b_accion = (isset($_POST['b_accion']) ? $_POST['b_accion'] : null);
$resultado = null;

echo '<section class="entradas">';

if($b_accion){
    $title_entrada = filter_input(INPUT_POST,'title_entrada');
    $description_entrada = filter_input(INPUT_POST, 'description_entrada');

    if ($title_entrada && $description_entrada){
        $resultado = Entrada::newEntrada($user->devolverValor('id'), $title_entrada, $description_entrada);
    }else{
        $resultado = Wrapper::wrap(400,'Por favor, se necesitan título y descripción.');
    }
    Message::printMessage($resultado);
}

$resultadoEntradas = Entrada::getAllSimpleEntradas();


PortadaView::printNewEntrada($resultado);
PortadaView::printEntradas($resultadoEntradas['data']['entradas']);
echo '</section>';