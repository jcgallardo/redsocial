<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 18:02
 */

require_once('models/User.php');
require_once('views/InfoView.php');

$resultadoFotos = null;

if ($otro_user != null){
    InfoView::printInfo($otro_user);
}else{
    echo '<section class="informacion">';
    $b_accion = filter_input(INPUT_POST, 'b_accion');
    $new_name = filter_input(INPUT_POST, 'name');
    $new_lastname = filter_input(INPUT_POST, 'last_name');
    $new_email = filter_input(INPUT_POST, 'email');

    if ($b_accion == "update_profile"){
        $resultado = User::updateProfile($user->devolverValor('id'), $new_name, $new_lastname, $new_email);
        Message::printMessage($resultado);
    }

    $resultadoInfo = User::getUserById($user->devolverValor('id'));
    $action = "index.php?section=".$seccion;
    InfoView::printInfoEditable($resultadoInfo['data']['usuario'],$action);
    echo "</section>";
}


