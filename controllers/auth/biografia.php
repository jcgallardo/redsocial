<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 03/05/2017
 * Time: 18:48
 */

include('models/Entrada.php');
include('views/BiografiaView.php');

$resultadoEntradas = null;

if ($otro_user != null){
    $resultadoEntradas = Entrada::getSimpleEntradasByIdUser($otro_usuario_id);
}else{
    $resultadoEntradas = Entrada::getSimpleEntradasByIdUser($user->devolverValor('id'));
}

BiografiaView::printEntradas($resultadoEntradas['data']['entradas']);