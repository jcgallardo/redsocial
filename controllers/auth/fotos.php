<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 13/05/2017
 * Time: 18:02
 */

include('models/Entrada.php');
include('views/FotosView.php');

$resultadoFotos = null;

if ($otro_user != null){
    $resultadoFotos = Entrada::getOnlyFotos($otro_usuario_id);
}else{
    $resultadoFotos = Entrada::getOnlyFotos($user->devolverValor('id'));
}

FotosView::printGaleria($resultadoFotos['data']['fotos']);