<?php
/**
 * Created by PhpStorm.
 * User: jcgallardo
 * Date: 14/05/2017
 * Time: 19:40
 */

require_once('models/Entrada.php');
require_once('models/Comentario.php');
require_once ('views/EntradaView.php');

$id_entrada = filter_input(INPUT_GET,'id');
$b_accion = filter_input(INPUT_POST, 'b_accion');

echo '<section class="contenido-izquierda">';

if ($id_entrada){
    // si se ha pulsado sobre nuevo comentario lo insertamos
    if ($b_accion == "new_comment"){
        $description = filter_input(INPUT_POST, 'description');
        if ($description != "") {
            $resultado_insercion = Comentario::mewComment($id_entrada, $user->devolverValor('id'), $description);
            Message::printMessage($resultado_insercion);
        }else{
            Message::printError("Lo sentimos, no podemos insertar un comentario vacío");
        }
    }

    $resultado_entrada = Entrada::getComplexEntradaById($id_entrada);
    if($resultado_entrada['code']==200) {
        EntradaView::printEntrada($resultado_entrada['data']['entrada']);

        $action = "index.php?section=".$seccion."&id=".$id_entrada;
        EntradaView::printComentarios($resultado_entrada['data']['comentarios'], $user, $action);
    }
}

echo '</section>';